def middleware(queryString):
    words = queryString.split(" ")
    swearWords = ["shit","fuck","cunt","bitch"]
    return ' '.join([x for x in words if x not in swearWords])