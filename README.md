# buscar-plugins

This repository houses all the plugins for the [buscar web browser](https://gitlab.com/shakna-israel/buscar).

They can be downloaded with ```buscar install {name}``` where ```{name}``` refers to the plugin name.

---

## Contributing

All plugins will be reviewed before being accepted, and the author reserves the right to reject a plugin if it doesn't fit the culture of buscar.

All plugins must comply with the [LICENSE](LICENSE)

Plugins should:

* Be a single file.
* Contain one function, which takes a string, and returns a string, called ```middleware```.
* Not do anything but modify the given string.

A plugin that modifies the query of a user, should be called a before middleware.

A plugin that modifies the HTML output of a site, should be called an after middleware.